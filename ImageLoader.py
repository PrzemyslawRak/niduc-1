def img2binary():
    with open("test_img.jpg", "rb") as image:
        img = image.read()
        # print(len(f64))
        binary = "".join(["{:08b}".format(x) for x in img])
        # print(binary)
        return binary

