import math
import numpy as np
import matplotlib.pyplot as plt
import random as rnd

from typing import List


def modulate_bpsk(binary, frequency, sample_freq):
    shift = 0
    signal: List[float] = []

    x_axis_for_each_bit = np.arange(sample_freq / frequency)

    for bit in binary:
        if bit == 1:
            shift = 0

        if bit == 0:
            shift = math.pi

        y = np.sin(2 * np.pi * frequency * (x_axis_for_each_bit / sample_freq) + shift)
        signal.extend(y)

    return signal


def demodulate_bpsk(signal, frequency, sample_freq):
    binary = []

    period = sample_freq // frequency

    x_axis = np.arange(period)
    model_wave_0 = np.sin(2 * np.pi * frequency * (x_axis / sample_freq) + math.pi)
    model_wave_1 = np.sin(2 * np.pi * frequency * (x_axis / sample_freq))

    for outer_loop_i in range(0, len(signal), period):
        sampled_wave = []
        for inner_loop_i in range(0, period):
            sampled_wave.append(signal[outer_loop_i + inner_loop_i])

        corr_with_0 = np.corrcoef(model_wave_0, sampled_wave)
        corr_with_1 = np.corrcoef(model_wave_1, sampled_wave)

        max_corr_value = max(corr_with_0[0][1], corr_with_1[0][1])

        if max_corr_value == corr_with_0[0][1]:
            binary.extend([0])
        elif max_corr_value == corr_with_1[0][1]:
            binary.extend([1])

    return binary


def modulate_qpsk(binary, frequency, sample_freq):
    shift = 0
    signal: List[float] = []

    if len(binary) % 2 == 0:
        x_axis_for_each_2_bits = np.arange(sample_freq / frequency)

        for i in range(0, len(binary), 2):
            odd_bit = binary[i]
            even_bit = binary[i + 1]

            if odd_bit == 0:
                shift = 0

            if odd_bit == 1:
                shift = math.pi

            y1 = np.sin(2 * np.pi * frequency * (x_axis_for_each_2_bits / sample_freq) + shift + math.pi/4)/(np.sqrt(2))

            if even_bit == 0:
                shift = 0

            if even_bit == 1:
                shift = math.pi

            y2 = np.cos(2 * np.pi * frequency * (x_axis_for_each_2_bits / sample_freq) + shift + math.pi/4)/(np.sqrt(2))

            y = y1 + y2

            signal.extend(y)

    return signal


def demodulate_qpsk(signal, frequency, sample_freq):
    binary = []

    period = sample_freq // frequency

    x_axis = np.arange(period)
    model_wave_00 = np.cos(2 * np.pi * frequency * (x_axis / sample_freq))
    model_wave_10 = np.cos(2 * np.pi * frequency * (x_axis / sample_freq) + 2 * math.pi / 4)
    model_wave_11 = np.cos(2 * np.pi * frequency * (x_axis / sample_freq) + 2 * math.pi / 2)
    model_wave_01 = np.cos(2 * np.pi * frequency * (x_axis / sample_freq) + 2 * 3 * math.pi / 4)

    for outer_loop_i in range(0, len(signal), period):
        sampled_wave = []
        for inner_loop_i in range(0, period):
            sampled_wave.append(signal[outer_loop_i+inner_loop_i])

        corr_with_00 = np.corrcoef(model_wave_00, sampled_wave)
        corr_with_10 = np.corrcoef(model_wave_10, sampled_wave)
        corr_with_11 = np.corrcoef(model_wave_11, sampled_wave)
        corr_with_01 = np.corrcoef(model_wave_01, sampled_wave)

        max_corr_value = max(corr_with_00[0][1], corr_with_10[0][1], corr_with_11[0][1], corr_with_01[0][1])

        if max_corr_value == corr_with_00[0][1]:
            binary.extend([0, 0])
        elif max_corr_value == corr_with_10[0][1]:
            binary.extend([1, 0])
        elif max_corr_value == corr_with_11[0][1]:
            binary.extend([1, 1])
        elif max_corr_value == corr_with_01[0][1]:
            binary.extend([0, 1])

    return binary


def modulate_16qam(binary, frequency, sample_freq):
    amplitude = 0
    signal: List[float] = []

    if len(binary) % 4 == 0:
        x_axis_for_each_pair = np.arange(sample_freq / frequency)

        for i in range(0, len(binary), 4):
            first_pair = [binary[i], binary[i+1]]
            second_pair = [binary[i+2], binary[i+3]]

            if first_pair == [0, 0]:
                amplitude = -0.22
            if first_pair == [0, 1]:
                amplitude = -0.821
            if first_pair == [1, 0]:
                amplitude = 0.22
            if first_pair == [1, 1]:
                amplitude = 0.821

            y1 = amplitude * np.sin(2 * np.pi * frequency * (x_axis_for_each_pair / sample_freq))

            if second_pair == [0, 0]:
                amplitude = -0.22
            if second_pair == [0, 1]:
                amplitude = -0.821
            if second_pair == [1, 0]:
                amplitude = 0.22
            if second_pair == [1, 1]:
                amplitude = 0.821

            y2 = amplitude * np.cos(2 * np.pi * frequency * (x_axis_for_each_pair / sample_freq))

            y = y1 + y2

            signal.extend(y)

    return signal

'''
def demodulate_16qam(binary, frequency, sample_freq):
    amplitude = 0
    signal: List[float] = []

    return signal '''


def add_noise(signal, max_deviation):
    new_signal: List[float] = []

    for value in signal:
        deviation = rnd.choice([-1, 1]) * rnd.random() * max_deviation  #max deviation oznacza maksymalne bezwględne odchylenie, tj nie jest to wartość procentowa
        value = value + deviation
        new_signal.append(value)

    return new_signal


def show_plot(signal, frequency, sample_freq, binary_length, bit_block_size):
    samples_quantity = (sample_freq / frequency) * binary_length / bit_block_size
    x = np.arange(samples_quantity)

    plt.plot(x, signal)
    plt.axhline(y=0, color='r')
    plt.show()

